<?php

if (!function_exists('show_active_menu'))
{
	function show_active_menu($slug) //функция для определения активной страницы с помощью сегмента и $slug
	{
		$ci=& get_instance(); //определяем переменную для вызова глобально

		$result="";

		//задаем хелпер для главной страницы
		if($ci->uri->segment(1,0)===$slug) //проверяем значение первого сегмента адреса
		{
			$result="class='active'";
		}

		//задаем хелпер для страниц третьего сегмента адреса
		if($ci->uri->segment(3,0)===$slug) //проверяем значение сегмента адреса
		{
			$result="class='active'";
		}

		if ($slug==='films' && $ci->uri->segment(1, 0)==='main' && $ci->uri->segment(2, 0)==='movie') {
			$result="class='active'";
		}
		
		return $result;
	}

}

if (!function_exists('getUserById'))
{
	function getUserById($user_id) //функция вывода user_name одной таблицы по параметрам другой
	{
		$ci=& get_instance(); //определяем переменную для вызова глобально
		
		$ci->load->model('dx_auth/users');//подключаем таблицу библиотеки авторизации dx_auth
		$query = $ci->users->get_user_by_id($user_id);
		$result = $query->row(); //get_user_by_id выводит данные формата table, мы переводим эти данные в формат row
		return $result;
	}
}
