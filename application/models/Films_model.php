<?php

/**
 * модель для работы с таблицей movie базы данных 
 */
class Films_model extends CI_Model
{
	
	public function __construct()
	{
		$this->load->database();
	}

	/*public function getFilmsByRating($limit)
	{
		$query = $this->db
			->order_by('rating', 'desc') //вызываем сотрировку по столбцу rating, где desc указывает что сортировка от большего к меньшему
			->where('category_id', 1) //указываем что данные в столбце category_id должны равняться 1
			->where('rating>', 0) //указываем что данные в столбце рейтинг должны быть больше 0
			->limit($limit) //указываем какой лимит на количество вывода данных
			->get('movie'); //указываем с какой таблицы брать данные

		return $query->result_array(); //возвращаем массив полученных данных
	}*/

	public function getFilms($slug=FALSE, $limit, $type, $order_by, $order_type, $rating) //создаем универсальную модель для получение с бд данных с указанием slug, указанием лимита вывода количества данных и указанием типа выводимых данных из category_id
	{
		if ($slug === FALSE) //проверяем что slug не задан
		{
			$query = $this->db
				->where('category_id', $type) //выбираем данные по категории
				->order_by($order_by, $order_type)
				->where('rating>', $rating)
				//->order_by('add_date', 'desc') //сортируем по дате начиная от свежих
				->limit($limit) //задаем лимит на вывод количества данных
				->get('movie'); //указываем название таблицы
			return $query->result_array(); //выводим массив с результатом
		}

		$query = $this->db->get_where('movie', array('slug'=>$slug)); //если slug задан, то выводим строку со slug
		return $query->row_array();
	}

	public function getFilmsPag($row_count, $offset, $type=1) //для пагинации
	{
		$query = $this->db
				->where('category_id', $type) //выбираем данные по категории
				->order_by('add_date', 'desc')
				->get('movie', $row_count, $offset); //параметры пагинации
			return $query->result_array();
	}

	public function updateMovie($edit_movie)
	{
		return $this->db->update('movie', $edit_movie, array('slug'=>$edit_movie['slug']));
	}

	public function deleteMovie($slug)
	{
		return $this->db->delete('movie', array('slug' => $slug));
	}

	public function setMovie($new_movie)
	{
		return $this->db->insert('movie', $new_movie);
	}

	public function search($q)
	{		
		$array_search = array('name' => $q, 'descriptions' => $q);
		$query = $this->db
			->or_like($array_search)
			->limit(100)
			->get('movie');
		return $query->result_array();
	}
}