<?php

/**
 * класс для работы с таблицей comments бд, для работы с комментариями к фильмам
 */
class Comments_model extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}

	public function getComments($movie_id, $limit, $moderate_status, $row_count, $offset, $id, $admin)
	{
		if ($id)
		{
			$query = $this->db
			//->where('movie_id', $movie_id)
			->where('id', $id)
			//->where('moderate_status', $moderate_status)
			//->limit($limit)
			->get('comments');
		}
		elseif ($admin)
		{
			$query = $this->db
			->where('movie_id', $movie_id)
			->limit($limit)
			->get('comments', $row_count, $offset);
		}
		else
		{
			$query = $this->db
			->where('movie_id', $movie_id)
			->where('moderate_status', $moderate_status)
			->limit($limit)
			->get('comments', $row_count, $offset);
		}
		return $query->result_array();
	}

	public function setComment($new_comment)
	{
		return $this->db->insert('comments', $new_comment);
	}

	public function editComment($moderate)
	{
		return $this->db->update('comments', $moderate, array('id'=>$moderate['id']));
	}

	public function deleteComment($id)
	{
		return $this->db->delete('comments', array('id'=>$id));
	}

	
}