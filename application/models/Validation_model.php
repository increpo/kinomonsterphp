<?php

class Validation_model extends CI_Model {

        public function valid()
        {
                $this->load->helper(array('form', 'url'));

                $this->load->library('form_validation');

                $this->config = array(
                        array(
                                'field' => 'slug',
                                'label' => 'Slug',
                                'rules' => array('trim', 'required', 'min_length[1]', 'max_length[20]', 'callback_slug_check'),
                                'errors' => array(
                                    'required' => 'You must provide a %s.'),
                        ),
                        array(
                                'field' => 'name',
                                'label' => 'Name',
                                'rules' => array('trim', 'required', 'min_length[1]', 'max_length[50]'),
                                'errors' => array(
                                    'required' => 'You must provide a %s.'),
                                //'errors' => array(
                                        //'required' => 'You must provide a %s.',
                        ),
                        array(
                                'field' => 'descriptions',
                                'label' => 'Descriptions',
                                'rules' => array('trim', 'required', 'min_length[1]', 'max_length[500]')
                        ), //'matches[password]'
                        array(
                                'field' => 'year',
                                'label' => 'year',
                                'rules' => array('trim', 'required', 'exact_length[4]')
                        ),
                        array(
                                'field' => 'rating',
                                'label' => 'rating',
                                'rules' => array('trim', 'required', 'less_than_equal_to[10]')
                        ),
                        array(
                                'field' => 'poster',
                                'label' => 'poster',
                                'rules' => array('trim', 'required', 'valid_url')
                        ),
                        array(
                                'field' => 'player_code',
                                'label' => 'player_code',
                                'rules' => array('trim', 'required', 'valid_url')
                        ),
                        array(
                                'field' => 'category_id',
                                'label' => 'category_id',
                                'rules' => array('trim', 'required', 'in_list[1,2]')
                        )
                );

 
                $this->form_validation->set_rules($this->config);

                  
        }

       
}

