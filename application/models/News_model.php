<?php

/**
 * модель для с таблицей news базы данных
 */
class News_model extends CI_Model //название модели обязательно должно быть одинаково с названием файла модели, а файл с большойбуквы :-)
{
	
	public function __construct()
	{
		$this->load->database(); //подключаем базу данных
	}

	public function getNews($slug = FALSE, $limit, $order_by)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->limit($limit)->order_by($order_by, 'desc')->get('news'); //выводим все новости, если переменная $slug не определена 
			return $query->result_array();
		}
		$query = $this->db->get_where('news', array('slug' => $slug)); //указываем что нужен возврат массива с бд где данные в столбце slug будут соответствовать переменной $slug
		return $query->row_array();
	}

	public function setNews($newnews)
	{
/*		$data = array(
			'slug' => $slug,
			'title' => $title,
			'text' => $text);*/
		return $this->db->insert('news', $newnews); //передаем данные в БД, таблицу news
	}

	public function updateNews($news_data)
	{

		return $this->db->update('news', $news_data, array('slug'=>$news_data['slug'])); //передаем данные в БД, таблицу news
	}

	public function deleteNews($slug)
	{
		return $this->db->delete('news', array('slug'=>$slug));
	}
}