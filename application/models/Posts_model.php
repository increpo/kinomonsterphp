<?php

/**
 * модель для работы с БД постов
 */
class Posts_model extends CI_Model
{
	
	public function __construct()
	{
		$this->load->database();
	}

	public function getPosts($slug=FALSE, $limit) //метод для отображения постов
	{
		if ($slug===FALSE)
		{
			$query = $this->db
				->limit($limit)
				->order_by('id', 'desc')
				->get('posts');
			return $query->result_array();
		}
		
		$query = $this->db->get_where('posts', array('slug' => $slug));
		return $query->row_array();

	}

	public function newPost($new_post) //метод для добавления новых постов
	{
		return $this->db->insert('posts', $new_post);
	}

	public function updatePost($post_data) //метод для обновления данных при редактировании
	{
		return $this->db->update('posts', $post_data, array('slug' => $post_data['slug']));
	}

	public function deletePost($slug) //метод для удаления данных при редактировании
	{
		return $this->db->delete('posts', array('slug' => $slug));
	}

	
}