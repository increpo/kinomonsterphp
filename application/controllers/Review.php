<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * контроллер главной страницы
 */
class Review extends CI_Controller
{

	public function __construct()
	{
		parent::__construct(); //используем родительскую функцию конструктора
	}

	public function comments_moderate()  //контроллер модерации отзывов о фильмах
	{

		$this->load->model('comments_model');
		$comments = $this->comments_model->getComments(FALSE, FALSE, FALSE, FALSE, FALSE, $_POST["id"], FALSE);
		$comment = $comments[0];
		//$comment_id = $comment['id'];
		$moderate_status = $comment['moderate_status'];


		if ($moderate_status) {
			$comment['moderate_status'] = 0;
			$this->comments_model->editComment($comment);
		} else {$comment['moderate_status'] = 1;
			$this->comments_model->editComment($comment);
		};


		if (isset($_POST["id"])) { 

			// Формируем массив для JSON ответа
		    $result = array(
		    	'id' => $_POST["id"],
		    	'moderate_status' => $moderate_status,
		    	//'comment_id' => $comment_id,
		    ); 

		    

		    // Переводим массив в JSON
		    echo json_encode($result); 
		};

	}

	public function new_review()  //добавление нового отзыва
	{

		$this->load->model('comments_model');
		if (!empty($_POST["new_comment_text"])) {
			$new_comment['comment_text'] = $_POST["new_comment_text"];
			$new_comment['user_id'] = $_POST["user_id"];
			$new_comment['movie_id'] = $_POST["movie_id"];

			$result = "Ошибка добавления отзыва";

			if($this->comments_model->setComment($new_comment)){
				$result = "Отзыв успешно отправлен на модерацию";
			}
		}
		
		if (isset($_POST["movie_id"])) { 

			// Формируем массив для JSON ответа
		    $result = array(
		    	'comment_text' => $_POST["new_comment_text"],
		    	'user_id' => $_POST["user_id"],
		    ); 

		    

		    // Переводим массив в JSON
		    echo json_encode($result); 
		};

	}

	public function del_review()  //удаление отзыва
	{
		$this->load->model('comments_model');
		if (!empty($_POST["id"])) {
			
			$result = "Ошибка удаления отзыва";

			if($this->comments_model->deleteComment($_POST["id"])){
				$result = "Отзыв успешно удален";
			}
		}

		if (isset($_POST["id"])) { 

			// Формируем массив для JSON ответа

		    $result = array(
		    	'id' => $_POST["id"],
		    	'result' => $result,
		    ); 


		    // Переводим массив в JSON
		  echo json_encode($result); 
		};

	}
}