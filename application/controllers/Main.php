<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * контроллер главной страницы
 */
class Main extends MY_Controller
{

	public function __construct()
	{
		parent::__construct(); //используем родительскую функцию конструктора
	}

	public function index()  //организуем отображение главной страницы
	{
		$this->data['title'] = "Главная страница";

		//$this->load->model('films_model'); //подключаем модель
		$this->data['films'] = $this->films_model->getFilms(FALSE, 8, 1, NULL, NULL, -1); //вызываем метод для отображения фильмов

		$this->data['serials'] = $this->films_model->getFilms(FALSE, 8, 2, NULL, NULL, -1); //вызываем метод для отображения сериалов

		$this->load->view('templates/header', $this->data);
		$this->load->view('main/index', $this->data);
		$this->load->view('templates/footer');
	}

	public function movie($slug=NULL)
	{
		$this->data['movie'] = $this->films_model->getFilms($slug, NULL, NULL, NULL, NULL, -1);

		if (empty($this->data['movie'])) {
			show_404();
		}

		$this->data['title'] = $this->data['movie']['name'];
		
		$this->load->library('pagination'); //подключаем библиотеку пагинации
		$this->data['comments'] = NULL;

		$offset = (int) $this->uri->segment(4);
		$row_count = 4; //указываем количество данных выводимые на одной странице
		$count = 0;

		$admin = 0;
		if ($this->dx_auth->is_admin()) {
			$admin = 1;
		}

		$this->load->model('comments_model');
		$this->data['comments'] = $this->comments_model->getComments($this->data['movie']['id'], FALSE, 1, $row_count, $offset, NULL, $admin);
		//$this->data['comments'] = $this->comments_model->getComments($this->data['movie']['id'], 20, FALSE, FALSE);
		
		$count = count($this->comments_model->getComments($this->data['movie']['id'], 9999, 1 || 0, FALSE, FALSE, NULL, $admin));
		$p_config['base_url']='/main/movie/'.$this->data['movie']['slug']; //переменная для конфигуриции пагинации

		if ($this->input->post('comment_text')) //оставить отзыв
		{
			$new_comment = array(
				'comment_text' => $this->input->post('comment_text'),
				'user_id' => $this->dx_auth->get_user_id(),
				'movie_id' => $this->data['movie']['id'],
			);
			$this->data['result'] = "Ошибка добавления отзыва";
			if ($this->comments_model->setComment($new_comment)) {
				$this->data['result'] = "Отзыв успешно отправлен на модерацию";
			}

		}

		elseif ($this->input->post('comment_id')) //модерация отзывов
		{
			$comment_array = $this->comments_model->getComments(FALSE, FALSE, FALSE, $row_count, $offset, $this->input->post('comment_id'), $admin);
			$comment = $comment_array['0'];
			
			$comment['moderate_status'] = $this->input->post('moderate_status');
			$this->data['comment'] = $comment_array;
			$this->data['result'] = "Ошибка модерации отзыва";
			if ($this->comments_model->editComment($comment))
			{
				$this->data['result'] = "изменения приняты";
			}

		}

		$this->data['comments'] = $this->comments_model->getComments($this->data['movie']['id'], FALSE, 1, $row_count, $offset, NULL, $admin);

		//конфигурируем пагинацию
		$p_config['total_rows'] = $count;
		$p_config['per_page'] = $row_count;

		$p_config['full_tag_open'] = "<ul class='pagination'>";
		$p_config['full_tag_close'] = "</ul>";
		$p_config['num_tag_open'] = "<li>";
		$p_config['num_tag_close'] = "</li>";
		$p_config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$p_config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$p_config['next_tag_open'] = "<li>";
		$p_config['next_tagl_close'] = "</li>";
		$p_config['prev_tag_open'] = "<li>";
		$p_config['prev_tagl_close'] = "</li>";
		$p_config['first_tag_open'] = "<li>";
		$p_config['first_tagl_close'] = "</li>";
		$p_config['last_tag_open'] = "<li>";
		$p_config['last_tagl_close'] = "</li>";


		//инициализация пагинации
		$this->pagination->initialize($p_config);

		$this->data['pagination'] = $this->pagination->create_links();

		$this->load->view('templates/header', $this->data);
		$this->load->view('main/movie', $this->data);
		$this->load->view('templates/footer');
	}

	public function type($slug=NULL)
	{
		$this->load->library('pagination'); //подключаем библиотеку пагинации

		$this->data['movie_data'] = NULL;

		$offset = (int) $this->uri->segment(4);
		$row_count = 4; //указываем количество данных выводимые на одной странице
		$count = 0;

		if ($slug == "films")
		{
			$count = count($this->films_model->getFilms(FALSE, 9999, 1, NULL, NULL, -1)); //считаем количество данных в таблице бд
			$p_config['base_url']='/main/type/films/'; //переменная для конфигуриции пагинации

			$this->data['title'] = "Фильмы";
			$this->data['movie_data'] = $this->films_model->getFilmsPag($row_count, $offset, 1);
		}

		if ($slug == "serials")
		{
			$count = count($this->films_model->getFilms(FALSE, 9999, 2, NULL, NULL, -1)); //считаем количество данных в таблице бд
			$p_config['base_url']='/main/type/serials/'; //переменная для конфигуриции пагинации
			$this->data['title'] = "Сериалы";
			$this->data['movie_data'] = $this->films_model->getFilmsPag($row_count, $offset, 2);
		}

		if ($this->data['movie_data'] == NULL)
		{
			show_404();
		}
		
		//конфигурируем пагинацию
		$p_config['total_rows'] = $count;
		$p_config['per_page'] = $row_count;

		$p_config['full_tag_open'] = "<ul class='pagination'>";
		$p_config['full_tag_close'] = "</ul>";
		$p_config['num_tag_open'] = "<li>";
		$p_config['num_tag_close'] = "</li>";
		$p_config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$p_config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$p_config['next_tag_open'] = "<li>";
		$p_config['next_tagl_close'] = "</li>";
		$p_config['prev_tag_open'] = "<li>";
		$p_config['prev_tagl_close'] = "</li>";
		$p_config['first_tag_open'] = "<li>";
		$p_config['first_tagl_close'] = "</li>";
		$p_config['last_tag_open'] = "<li>";
		$p_config['last_tagl_close'] = "</li>";

		//инициализация пагинации
		$this->pagination->initialize($p_config);

		$this->data['pagination'] = $this->pagination->create_links();

		$this->load->view('templates/header', $this->data);
		$this->load->view('main/type', $this->data);
		$this->load->view('templates/footer');
	}

	/*public function serials()
	{
		$this->data['movies'] = $this->films_model->getFilms(FALSE, 9999, NULL, NULL, NULL, -1);
		//if
		$this->data['title'] = "Сериалы";

		$this->load->view('templates/header', $this->data);
		$this->load->view('main/serials', $this->data);
		$this->load->view('templates/footer');
	}*/
}