<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * контроллера поисковика
 */
class Search extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct(); //используем родительскую функцию конструктора
	}

	public function index()
	{
		$this->data['title'] = "Поиск";

		$q = $this->input->get('q_search');
		$this->data['search_results'] = $this->films_model->search($q);

		$this->load->view('templates/header', $this->data);
		$this->load->view('search', $this->data);
		$this->load->view('templates/footer');
	}
}