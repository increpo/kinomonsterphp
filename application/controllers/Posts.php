<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * контроллер для работы с постами
 */
class Posts extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct(); //используем родительскую функцию конструктора
		$this->load->model('posts_model'); //подключаем нашу модель posts_model
	}

	public function newPost()
	{
		$this->data['title'] = "создание поста";

		if ($this->input->post('slug') && $this->input->post('title') && $this->input->post('content'))
		{
			$new_post['slug'] = $this->input->post('slug');
			$new_post['title'] = $this->input->post('title');
			$new_post['content'] = $this->input->post('content');

			if ($this->posts_model->newPost($new_post))
			{
				$this->load->view('templates/header', $this->data);
				$this->load->view('posts/postSuccess', $this->data);
				$this->load->view('templates/footer');
			}
		}
		else
		{
			$this->load->view('templates/header', $this->data);
			$this->load->view('posts/newPost', $this->data);
			$this->load->view('templates/footer');
		}
		
	}

	public function index()
	{
		$this->data['title'] = "Все посты: ";

		$this->data['all_posts'] = $this->posts_model->getPosts(FALSE, NULL);

		$this->load->view('templates/header', $this->data);
		$this->load->view('posts/index', $this->data);
		$this->load->view('templates/footer');
	}

	public function editPost($slug=NULL)
	{
		$this->data['title'] = "Редатирование поста";
		$this->data['post_data'] = $this->posts_model->getPosts($slug, NULL); //вывод данных в форму

		if ($this->input->post('slug') && $this->input->post('title') && $this->input->post('content')) //проверка заполнения всех форм данными
		{
			$this->post_data['slug'] = $this->input->post('slug');
			$this->post_data['title'] = $this->input->post('title');
			$this->post_data['content'] = $this->input->post('content');

			if ($this->posts_model->updatePost($this->post_data))
			{
				$this->load->view('templates/header', $this->data);
				$this->load->view('posts/postSuccess', $this->data);
				$this->load->view('templates/footer');
			}
		}
		else
		{
			$this->load->view('templates/header', $this->data);
			$this->load->view('posts/editPost', $this->data);
			$this->load->view('templates/footer');			
		}
	}

	public function deletePost($slug = NULL)
	{
		$this->data['title'] = "Удаление поста";
		$this->deletePostSlug = $this->posts_model->getPosts($slug, NULL);

		if (empty($this->deletePostSlug['slug']))
		{
			show_404();
		}

		if ($this->posts_model->deletePost($this->deletePostSlug['slug']))
		{
			$this->load->view('templates/header', $this->data);			
			$this->load->view('posts/postSuccess', $this->data);
			$this->load->view('templates/footer');
		}
	}
	
	
}