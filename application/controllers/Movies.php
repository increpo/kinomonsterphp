<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * контроллер для добавления, редактирования и удаления фильмов и сериалов
 */
class Movies extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('films_model');
	}

	public function edit($slug=FALSE)
	{
		$this->data['title'] = "Редактирование фильмов";

		$this->data['movie'] = $this->films_model->getFilms($slug, NULL, NULL, NULL, NULL, -1);

		if(!$this->dx_auth->is_admin())
		{
			show_404();
		}
		
		if(empty($this->data['movie'])){
			show_404();
		}

		if ($this->input->post('slug') && $this->input->post('name') && $this->input->post('descriptions') && $this->input->post('year') && $this->input->post('rating') && $this->input->post('poster') && $this->input->post('player_code') && $this->input->post('category_id'))
		{
			$edit_movie = array(
					'slug' => $this->input->post('slug'),
					'name' => $this->input->post('name'),
					'descriptions' => $this->input->post('descriptions'),
					'year' => $this->input->post('year'),
					'rating' => $this->input->post('rating'),
					'poster' => $this->input->post('poster'),
					'player_code' => $this->input->post('player_code'),
					'category_id' => $this->input->post('category_id')
				);

			$this->data['result'] = "Ошибка редактирования.";

			if ($this->films_model->updateMovie($edit_movie))
			{
				$this->data['result'] = "Видео: ".$this->data['movie']['name']." успешно отредактировано!";

				$this->load->view('templates/header', $this->data);
				$this->load->view('/movies/success', $this->data);
				$this->load->view('templates/footer');
			}
		}
		else
		{
			$this->load->view('templates/header', $this->data);
			$this->load->view('/movies/edit', $this->data);
			$this->load->view('templates/footer');
		}
	}

	public function delete($slug=FALSE)
	{
		if(!$this->dx_auth->is_admin())
		{
			show_404();
		}

		$this->data['title'] = "удаление видео";

		$this->data['movie'] = $this->films_model->getFilms($slug, NULL, NULL, NULL, NULL, -1);

		if(empty($this->data['movie']))
		{
			show_404();
		}

		$this->data['result'] = "Ошибка удаления";

		if ($this->films_model->deleteMovie($slug)) {
			$this->data['result'] = "Видео: ".$this->data['movie']['name']." успешно удалено!";
		}

		$this->load->view('templates/header', $this->data);
		$this->load->view('/movies/success', $this->data);
		$this->load->view('templates/footer');
	}

	public function create()
	{
		$this->data['title'] = "создание нового видео";

		if (!$this->dx_auth->is_admin())
		{
			show_404();
		}


        $this->load->model('validation_model');

        $this->validation_model->valid();

		//$this->load->model('films_model');
        $film_slug = $this->films_model->getFilms(FALSE, 10000, 1, NULL, NULL, -1);
		$this->data['film_slug'] = $film_slug; //для просмотра данных массива

        if ($this->form_validation->run() == FALSE)
        {
                $this->load->view('templates/header', $this->data);
				$this->load->view('/movies/create', $this->data);
				$this->load->view('templates/footer');
        }
        else
        {
                
		$this->data['result'] = "Ошибка добавления нового видео";

		$new_movie = array(
				'slug' => $this->input->post('slug'),
				'name' => $this->input->post('name'),
				'descriptions' => $this->input->post('descriptions'),
				'year' => $this->input->post('year'),
				'rating' => $this->input->post('rating'),
				'poster' => $this->input->post('poster'),
				'player_code' => $this->input->post('player_code'),
				'category_id' => $this->input->post('category_id')
				);

			if ($this->films_model->setMovie($new_movie))
			{
				$this->data['result'] = "Новое видео ".$new_movie['name']." успешно добавлено!";
			}

			$this->load->view('templates/header', $this->data);
			$this->load->view('/movies/success', $this->data);
			$this->load->view('templates/footer');

                       // $this->load->view('formsuccess');                       
        }



		/*if ($this->input->post('slug') && $this->input->post('name') && $this->input->post('descriptions') && $this->input->post('year') && $this->input->post('rating') && $this->input->post('poster') && $this->input->post('player_code') && $this->input->post('category_id'))
		{
			$this->data['result'] = "Ошибка добавления нового видео";

			$new_movie = array(
					'slug' => $this->input->post('slug'),
					'name' => $this->input->post('name'),
					'descriptions' => $this->input->post('descriptions'),
					'year' => $this->input->post('year'),
					'rating' => $this->input->post('rating'),
					'poster' => $this->input->post('poster'),
					'player_code' => $this->input->post('player_code'),
					'category_id' => $this->input->post('category_id')
					);

			if ($this->films_model->setMovie($new_movie))
			{
				$this->data['result'] = "Новое видео ".$new_movie['name']." успешно добавлено!";
			}
		}
		else
		{
			$this->load->view('templates/header', $this->data);
			$this->load->view('/movies/create', $this->data);
			$this->load->view('templates/footer');
		}*/
	}


	 public function slug_check($str) //для валидации поля с данными бд
        {
                
               $this->load->model('films_model');
                $film_slug = $this->films_model->getFilms(FALSE, 10000, 1, NULL, NULL, -1);

                foreach ($film_slug as $key => $value)
                {
                        if ($str == $value['slug'])
                        {
                                $this->form_validation->set_message('slug_check', 'Such {field} is already exists, plese think of another one');
                                return FALSE;
                        }

                }
                                return TRUE;
                        
        }

}