<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * класс для логики вывода, редактирования и удаления новостей
 */
class News extends MY_Controller  //заменил наследование от главного контроллера кодэгнитора CI_Controller на наш контроллер MY_Controller, который будет сам наследоваться от контроллера CI_Controller и, в свою очередь соединит модели нескольких разных контроллеров. для видимости тегов данных необходимо изменить $data на $this->data
{

	public function __construct()
	{
		parent::__construct(); //используем родительскую функцию конструктора
		$this->load->model('news_model'); //подключаем нашу модель News_model
	}

	public function index()  //организуем отображение страницы просмотра всех новостей
	{
		$this->data['title'] = "Все новости";
		$this->data['news'] = $this->news_model->getNews(FALSE, NULL, NULL); //загружаем с базы данных данные для страницы, по выборке slug, которая реализована в модели

		$this->load->view('templates/header', $this->data);
		$this->load->view('news/index', $this->data);
		$this->load->view('templates/footer');
	}

	public function view($slug = NULL) //организуем отображение каждой новости с передачей ссылки $slug каждой новости
	{
		$this->data['news_item'] = $this->news_model->getNews($slug, NULL, NULL); //выгружаем даные в нашу переменную news_item с передачей $slug
		if(empty($this->data['news_item'])) //делаем проверку что переходят по имеено по ссылке $slug, которая имеется в базе данных
		{
			show_404();
		}

		$this->data['title'] = $this->data['news_item']['title']; //создаем переменную для заголовка из полученного массива news_item
		$this->data['content'] = $this->data['news_item']['text']; //создаем переменную для получения текста новости из полученного массива news_item

		//подключаем виды для страниц:
		$this->load->view('templates/header', $this->data);
		$this->load->view('news/view', $this->data);
		$this->load->view('templates/footer');
	}

	public function create() //создаем контроллер для добавления новостей из страницы в базу данных
	{
		if(!$this->dx_auth->is_admin())
		{
			show_404();
		}

		$this->data['title'] = "Добавить новость";

		if ($this->input->post('slug') && $this->input->post('title') && $this->input->post('text')) //проверяем заполнение всех полей формы
		{
			$newnews['slug'] = $this->input->post('slug'); //если все поля формы заполнены, передаем их в модель
			$newnews['title'] = $this->input->post('title');
			$newnews['text'] = $this->input->post('text');

			if ($this->news_model->setNews($newnews)) {
				//подключаем виды для страницы success в случае если все поля формы заполнены и данные отправлены в базу данных:
				$this->load->view('templates/header', $this->data);
				$this->load->view('news/success', $this->data);
				$this->load->view('templates/footer');
			}
		} else {
		//подключаем виды для страницы create так как не все поля заполнены или переход осуществлен впервые:
		$this->load->view('templates/header', $this->data);
		$this->load->view('news/create', $this->data);
		$this->load->view('templates/footer');
		}
	}

	public function edit($slug=NULL) //функция для редактирования новостей
	{
		$this->data['title'] = "Редактирование новостей";

		$this->data['news_item'] = $this->news_model->getNews($slug, NULL, NULL);
		
		if ($this->input->post('slug') && $this->input->post('title') && $this->input->post('text')) //проверяем заполнение всех полей формы
		{
			$this->news_data['slug'] = $this->input->post('slug');
			$this->news_data['title'] = $this->input->post('title');
			$this->news_data['text'] = $this->input->post('text');

			if ($this->news_model->updateNews($this->news_data)) //подключаем виды для страницы success в случае если все поля формы заполнены и данные отправлены в базу данных: 
			{
				$this->load->view('templates/header', $this->data);
				$this->load->view('news/updateSuccess', $this->data);
				$this->load->view('templates/footer');
			}

		} else //подключаем виды для страницы edit так как не все поля заполнены или переход осуществлен впервые:
		{
			$this->load->view('templates/header', $this->data);
			$this->load->view('news/edit', $this->data);
			$this->load->view('templates/footer');
		}
	}

	public function delete($slug=NULL)
	{
		$this->data['title'] = "удаление новости";

		$this->data['news_delete'] = $this->news_model->getNews($slug, NULL, NULL);

		if(empty($this->data['news_delete']))
		{
			show_404();
		}

		$this->data['result'] = "Ошибка удаления".$this->data['news_delete']['title'];

		if ($this->news_model->deleteNews($slug)) {
			$this->data['result'] = $this->data['news_delete']['title']. " успешно удалена";
		}

		$this->load->view('templates/header', $this->data);
		$this->load->view('news/delete', $this->data);
		$this->load->view('templates/footer');
	}


}