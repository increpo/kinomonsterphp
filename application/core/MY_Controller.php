<?php

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->data['title'] = "КиноМонстр - сайт о кино"; //title по умолчанию, на случае если он где-то будет не определен

		$this->load->model('news_model'); //загружаем модель для отображения новостей в левой колонки меню, которая будет повторяться на всех страниц сайта
		$this->data['limit_news'] = $this->news_model->getNews(FALSE, 7, "id"); //получаем все новости с метода getNews модели news_model

		$this->load->model('films_model'); //загружаем модель с работой с фильмами
		$this->data['films_rating'] = $this->films_model->getFilms(FALSE, 7, 1, "rating", "desc", 0); //вызываем метод из модели фильмов для получения данных
		$this->data['serials_rating'] = $this->films_model->getFilms(FALSE, 7, 2, "rating", "desc", 0); //вызываем метод из модели фильмов для получения данных


		$this->load->model('posts_model'); //загружаем модель постов для вывода их на главной странице
		$this->data['postsLimited'] = $this->posts_model->getPosts(FALSE, 2);
	}
}