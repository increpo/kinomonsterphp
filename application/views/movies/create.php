<?php echo validation_errors(); ?>

<?php echo form_open('/movies/create'); ?>


<form method="post">
	<input class="form-control input-lg" type="input" name="slug" placeholder="slug" value="<?php echo set_value('slug'); ?>"><br>
	<input class="form-control input-lg" type="input" name="name" placeholder="Название видео" value="<?php echo set_value('name'); ?>"><br>
	<textarea class="form-control input-lg" type="text" name="descriptions" placeholder="Описание видео"><?php echo set_value('descriptions'); ?></textarea><br>
	<input class="form-control input-lg" type="input" name="year" placeholder="Год премьеры" value="<?php echo set_value('year'); ?>"><br>
	<input class="form-control input-lg" type="input" name="rating" placeholder="Рейтинг" value="<?php echo set_value('rating'); ?>"><br>
	<input class="form-control input-lg" type="input" name="poster" placeholder="Ссылка на постер" value="<?php echo set_value('poster'); ?>"><br>
	<input class="form-control input-lg" type="input" name="player_code" placeholder="Ссылка на видео" value="<?php echo set_value('player_code'); ?>"><br>
	<input class="form-control input-lg" type="input" name="category_id" placeholder="ID категории" value="<?php echo set_value('category_id'); ?>"><br>
	<input class="btn btn-warning" type="submit" value="Добавить">
</form>



<!-- <form method="post">
	<input class="form-control input-lg" type="input" name="slug" placeholder="slug"><br>
	<input class="form-control input-lg" type="input" name="name" placeholder="Название видео"><br>
	<textarea class="form-control input-lg" type="text" name="descriptions" placeholder="Описание видео"></textarea><br>
	<input class="form-control input-lg" type="input" name="year" placeholder="Год премьеры"><br>
	<input class="form-control input-lg" type="input" name="rating" placeholder="Рейтинг"><br>
	<input class="form-control input-lg" type="input" name="poster" placeholder="Ссылка на постер""><br>
	<input class="form-control input-lg" type="input" name="player_code" placeholder="Ссылка на видео"><br>
	<input class="form-control input-lg" type="input" name="category_id" placeholder="ID категории"><br>
	<input class="btn btn-warning" type="submit" value="Добавить">
</form> -->