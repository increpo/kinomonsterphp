<h1><?php echo $title; ?></h1>
<a href="/movies/create" class="btn btn-warning btn-lg <?php if (!$this->dx_auth->is_admin()) {echo "hidden";} ?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить новое видео</a>
<hr>
<?php foreach ($movie_data as $key => $value): ?>
  <div class="row">
    <div class="well clearfix">
      <div class="col-lg-3 col-md-2 text-center">
        <img class="img-thumbnail" src="<?php echo $value['poster'] ?>" alt="<?php echo $value['name'] ?>">
        <p><?php echo $value['name'] ?></p>
      </div>
      <div class="col-lg-9 col-md-10">
        <div class="pull-right <?php if (!$this->dx_auth->is_admin()) {echo "hidden";} ?>">
          <a href="/movies/edit/<?php echo $value['slug']; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a><span>  |  </span>
          <a href="/movies/delete/<?php echo $value['slug']; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
        </div>
        <p>
          <?php echo $value['descriptions'] ?>
        </p>
      </div>
      <div class="col-lg-12">
        <a href="/main/movie/<?php echo $value['slug'] ?>" class="btn btn-warning btn-lg pull-right">подробнее</a>
      </div>
    </div>
  </div>
<?php endforeach ?>


<?php echo $pagination; ?>
  


<div class="margin-8"></div>

<!-- <nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
     <?php echo $pagination; ?>
    </li>
  </ul>
</nav> -->

