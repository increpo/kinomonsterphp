<h1><?php echo $movie['name']; ?></h1>	

<hr>

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="<?php echo $movie['player_code']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<div class="well info-block text-center">
  Год: <span class="badge"><?php echo $movie['year']; ?></span>
  Рейтинг: <span class="badge"><?php echo $movie['rating']; ?></span>
  Режисер: <span class="badge"><?php echo $movie['name']; ?></span>
</div>

<div class="margin-8"></div>

<h2>Описание <?php echo $movie['name']; ?></h2>
<hr>
<div class="well">
  <?php echo $movie['descriptions']; ?>
</div>

<div class="margin-8"></div>

<h2>Отзывы о <?php echo $movie['name']; ?></h2>
<hr>

<?php echo $pagination; ?>

<div class="panel panel-info">
  <?php if (!empty($result)): ?>
       <?php echo $result; ?>
  <?php endif ?>
  <?php foreach ($comments as $key => $value): ?> <!-- отображаем отзывы -->
    <div class="panel-heading" id="h<?php echo $value['id']; ?>"><i class="glyphicon glyphicon-user"></i><span> <?php echo getUserById($value['user_id'])->username; ?></span></div>
    <div class="panel-body <?php if (!$value['moderate_status']): ?><?php echo "moderate_status"; ?><?php endif ?>" id="c<?php echo $value['id']; ?>" >
     
      <?php if ($this->dx_auth->is_admin()): ?> <!-- отображение кнопок модерации отзывов -->
                
          <button class="moderate pull-right" id="<?php echo $value['id']; ?>0"><span class=" glyphicon glyphicon-pencil" aria-hidden="true"></span></button>
          <button class="delete pull-right" id="<?php echo $value['id']; ?>"><span class=" glyphicon glyphicon-trash" aria-hidden="true"></span></button><br>

      <?php endif ?> 


      <?php echo $value['comment_text']; ?>
    </div>
  <?php endforeach ?>


</div>

<div id="result_form"></div>
<!-- добавление отзывов -->
<?php if (!$this->dx_auth->is_admin()): ?>  

 
    <form method="post" class="ajax_new_review_form" action="" >
      <div class="form-group">
        <div class="panel panel-info">
          <div class="panel-heading">
            <i class="glyphicon glyphicon-user"></i>
            <span>
              <?php echo $this->dx_auth->get_username(); ?>        
            </span>
          </div>
            <div id="result_form_new_comment"></div>
        </div>
      </div>

      <div class="form-group">
        <textarea class="form-control input-lg" id="new_comment_text" name="new_comment_text"><?php if (!$this->dx_auth->is_logged_in()): ?>Для добавления отзыва необходимо зарегистрироваться.<?php endif ?></textarea>
        <input type="hidden" name="movie_id" value="<?php echo $movie['id']; ?>">
        <input type="hidden" name="user_id" value="<?php echo $this->dx_auth->get_user_id(); ?>">     
      </div>

      <?php if ($this->dx_auth->is_logged_in()): ?> <!-- добавление отзывов для зарегистрированных пользователей -->
        <?php if (!empty($result)): ?>
           <?php echo $result; ?>
        <?php endif ?>  
        <button class="btn btn-lg btn-warning pull-right" type = "submit" id="send">Отправить</button>

      <?php endif ?> 
    </form>
<?php endif ?>

    <div class="margin-8"></div>


   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <script src="/assets/js/ajax.js"></script>     



<!-- <?php print_r($comment); ?> -->