<h2>Новые фильмы</h2>
<hr> <!-- полоска -->

<div class="row">

  	<?php foreach ($films as $key => $value): ?>
		<div class="films_block col-lg-3 col-md-3 col-sm-3 col-xs-6">
		    <a href="/main/movie/<?php echo $value['slug'] ?>"><img src="<?php echo $value['poster']; ?>" alt="<?php echo $value['name']; ?>"></a>
		    <div class="film_label"><a href="/main/movie/<?php echo $value['slug'] ?>"><?php echo $value['name']; ?></a></div><br>
		</div>
	<?php endforeach ?>

</div>

 <div class="margin-8"></div>

 <h2>Новые сериалы</h2>
 <hr> <!-- полоска -->  

<div class="row">

  	<?php foreach ($serials as $key => $value): ?>
		<div class="films_block col-lg-3 col-md-3 col-sm-3 col-xs-6">
		    <img src="<?php echo $value['poster']; ?>" alt="<?php echo $value['name']; ?>">
		    <div class="film_label"><a href="/main/movie/<?php echo $value['slug'] ?>"><?php echo $value['name']; ?></a></div><br>
		</div>
	<?php endforeach ?>

</div> <!-- row -->

<div class="margin-8"></div> 

<?php foreach ($postsLimited as $key => $value): ?>
	<a href="/posts/"><h3> <?php echo $value['title']; ?> </h3></a>
<hr>
<p><?php echo $value['content']; ?></p>
<?php endforeach ?>

<div class="margin-8"></div>

<div class="text-center">
	<form action="/posts/"><input type="submit" class="btn btn-warning" value="все посты"></form>
</div>

<div class="margin-8"></div>
 