<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- включаем совместимость для старых версий эксплорера -->
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- включаем способность отображения страниц на мобильных устройствах -->
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title ?></title>

    <!-- Bootstrap -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"> <!-- указываем адрес к библиотеке ссs -->
     <link href="/assets/css/style.css" rel="stylesheet"> <!-- указываем адрес к библиотеке ссs, которую создали сами-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="container-fluid">
      <div class="row"> <!-- убираем отступы (поля) -->
       
        <nav role="navigation" class="navbar navbar-inverse">

          <div class="container">
            <div class="navbar-header header"> <!-- включаем созданный нами класс header -->

              <div class="container">
                <div class="row">
                  <div class="col-lg-12">
                    <h1><a href="/">КиноМонстр</a></h1>
                    <p>Кино наша страсть!</p>                    
                  </div>
                </div>
              </div>

              <button type="button" data-target="#navbarCollapse"
              data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span> <!-- создание меню для скрытия и отображения кнопок на экране с маленьким разрешением -->
                <span class="icon-bar"></span> <!-- полосочки на кнопке меню -->
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

            </div>            
          </div>

          <div id="navbarCollapse" class="collapse navbar-collapse navbar-right">
            <ul class="nav nav-pills">
              <li <?php echo show_active_menu(0); ?>> <a href="/">Главная</a> </li>
              <li <?php echo show_active_menu('films'); ?>> <a href="/main/type/films/">Фильмы</a> </li>
              <li <?php echo show_active_menu('serials'); ?>> <a href="/main/type/serials/">Сериалы</a> </li>
              <li <?php echo show_active_menu('rating'); ?>> <a href="rating.html">Рейтинг фильмов</a> </li>
              <li <?php echo show_active_menu('contacts'); ?>> <a href="#">Контакты</a> </li>
            </ul>            
          </div>
          
        </nav>

      </div>
    </div>

<!-- закончилась основная часть сайта, началось левое статичное меню, которое тоже можно добавить в хедер -->
    <div class="wrapper"> <!-- класс основной части, между хедером, меню и футером -->
      <div class="container">
        <div class="row">
          <div class="col-lg-9 col-lg-push-3">

            <div class="panel panel-info visible-xs"> <!-- панель поска вверху для смартфонов -->
              <div class="panel-body">
                <form role="search">
                  <div class="form-group"> <!-- для применения отступов для форм -->
                    <div class="input-group"> <!-- для того, чтобы можно было в класс формы вставить класс инпут -->
                      <input type="search" class="form-control input-lg" placeholder="Ваш запрос">
                      <div class="input-group-btn">
                        <button class="btn btn-default btn-lg" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                      </div>
                    </div>
                  </div>                  
                </form>
              </div>
            </div>