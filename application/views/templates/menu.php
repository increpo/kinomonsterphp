<div class="col-lg-3 col-lg-pull-9">

  <div class="panel panel-info hidden-xs">

    <div class="panel-heading">
      <div class="sidebar-header">Поиск</div>
    </div>

    <div class="panel-body">
      <form role="search" method="get" action="/search/">
        <div class="form-group"> <!-- для применения отступов для форм -->
          <div class="input-group"> <!-- для того, чтобы можно было в класс формы вставить класс инпут -->
            <!-- <span class="input-group-addon">@</span> -->
            <input type="search" class="form-control input-lg" placeholder="Ваш запрос" name="q_search">
            <div class="input-group-btn">
              <button class="btn btn-default btn-lg" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
          </div>
        </div>                  
      </form>
    </div> <!-- panel-body -->

  </div> <!-- panel panel-info hidden-xs -->

  <div class="panel panel-info">

    <div class="panel-heading">
      <div class="sidebar-header">Авторизация</div>
    </div>

    <div class="panel-body">

      <?php if (!$this->dx_auth->is_logged_in()): ?>        
      
        <form role="form" method="post" action="/auth/login/">

          <div class="form-group">
            <input type="text" name="username" class="form-control input-lg" placeholder="Логин">
          </div>

          <div class="form-group">
            <input type="password" name="password" class="form-control input-lg" placeholder="Пароль"> 
          </div>

          <button type="submit" class="btn btn-warning pull-right">Вход</button>                   
        </form>
      <?php else: ?>
        Здравствуйте, <?php echo $this->dx_auth->get_username(); ?>
        <a href="/auth/logout/" class="btn btn-warning pull-right">выход</a>
      <?php endif ?>

    </div> <!-- panel-body -->

  </div> <!-- panel panel-info -->

  <div class="panel panel-info"> <!-- панель новостей -->
    <div class="panel-heading"><div class="sidebar-header"><a href="/news/">Новости</a></div></div>
    <div class="panel-body">
      <?php foreach ($limit_news as $key => $value): ?>
        <p><a href="/news/view/<?php echo $value['slug']; ?>"><?php echo $value['title']; ?></a></p>
      <?php endforeach ?>
    </div>
  </div>

  <div class="panel panel-info">

    <div class="panel-heading">
      <div class="sidebar-header">Рейтинг фильмов</div>
    </div>

    <div class="panel-body">
      <ul class="list-group">
        <?php foreach ($films_rating as $key => $value): ?>
          <li class="list-group-item list-group-item-warning">
            <span class="badge"><?php echo $value['rating']; ?></span>
              <a href="/main/movie/<?php echo $value['slug'] ?>"><?php echo $value['name']; ?></a>
          </li>
        <?php endforeach ?>
      </ul>
    </div>
  </div> <!-- panel panel-info -->

 <div class="panel panel-info">

    <div class="panel-heading">
      <div class="sidebar-header">Рейтинг сериалов</div>
    </div>

    <div class="panel-body">
      <ul class="list-group">
        <?php foreach ($serials_rating as $key => $value): ?>
          <li class="list-group-item list-group-item-warning">
            <span class="badge"><?php echo $value['rating']; ?></span>
              <a href="/main/movie/<?php echo $value['slug'] ?>"><?php echo $value['name']; ?></a>
          </li>
        <?php endforeach ?>
      </ul>
    </div>

  </div> <!-- panel panel-info -->

</div> <!-- col-lg-3 col-lg-pull-9 -->