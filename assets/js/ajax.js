/* Article FructCode.com */
$( document ).ready(function() {

    $(".moderate").click(
        function(){
            console.log(this.id);
            let id = this.id/10;
            console.log(id);
            sendAjaxModerateForm('result_form', id, '/Review/comments_moderate');
            return false; 
        }
    );

    $("#send").click(
        function(){
            sendNewReview('result_form_new_comment', 'ajax_new_review_form', '/Review/new_review');
            return false; 
        }
    );

    $(".delete").click(
        function(){
            console.log(this.id);
            let id = this.id;
            let ask = confirm("Вы точно хотите удалить этот отзыв?");
            if (ask) {
            delReview('result_form', id , '/Review/del_review');
            return false;
            }
        }
    );
});
 
function sendAjaxModerateForm(result_form, id, url) {
    $.ajax({
        url:     url, //url страницы (action_ajax_form.php)
        type:     "POST", //метод отправки
        dataType: "html", //формат данных
        data: {id},  
        success: function(response) { //Данные отправлены успешно
            result = $.parseJSON(response);
            //$('#result_form').html('ID '+result.id+'<br>Чекбокс: '+result.moderate_status+result.comment_id);

            if (result.moderate_status==1) {
                let div = document.querySelectorAll("[id='c"+ result.id +"']");
                div[0].style.background = "gray";

            };
            if (result.moderate_status==0) {
                let div = document.querySelectorAll("[id='c"+ result.id +"']");
                div[0].style.background = "#f9f9f9";
            };

        },
        error: function(response) { // Данные не отправлены
            $('#result_form').html('Ошибка. Данные не отправлены.');
        }
    });

};


$('.moderate_status').css('background-color', 'gray');

function sendNewReview(result_form, ajax_moderate_form, url) {
    $.ajax({
        url:     url, //url страницы (action_ajax_form.php)
        type:     "POST", //метод отправки
        dataType: "html", //формат данных
        data: $("."+ajax_new_review_form).serialize(),  // Сеарилизуем объект
        success: function(response) { //Данные отправлены успешно
            result = $.parseJSON(response);
            //$('#result_form').html('ID '+result.id+'<br>Чекбокс: '+result.moderate_status+result.comment_id);

            $('#result_form_new_comment').html('Ваш отзыв <br>'+result.comment_text+'<br> отправлен на модерацию!');
            $('#result_form_new_comment').css('background-color', 'gray');
            $('#new_comment_text').val("");
            
        },
        error: function(response) { // Данные не отправлены
            $('#result_form_new_comment').html('Ошибка. Данные не отправлены.');
        }
    });

};

function delReview(result_form, id, url) {
    $.ajax({
        url:     url, //url страницы (action_ajax_form.php)
        type:     "POST", //метод отправки
        dataType: "html", //формат данных
        data: {id},  // Сеарилизуем объект
        success: function(response) { //Данные отправлены успешно
            result = $.parseJSON(response);

          // $('#result_form').html('ID '+result.id+result.result);
           $('#h'+result.id).remove();
           $('#c'+result.id).remove();
            
        },
        error: function(response) { // Данные не отправлены
            $('#result_form').html('Ошибка. Данные не отправлены.');
        }
    });

};